#!/bin/bash
echo "#### Custom config start. ####"
declare -a StringArray=("node1" "node2")
con="\n"

cp /amq/scripts/broker.xml ${CONFIG_INSTANCE_DIR}/etc/broker.xml

for val in ${StringArray[@]}; do
   if [ "$val-ss-0" == "$HOSTNAME" ]; then
     echo hiding $val to the cluster
     echo configuring local node connector-ref
     sed -i "s/LOCAL_CONNECTOR/$val-connector/g" ${CONFIG_INSTANCE_DIR}/etc/broker.xml
   else
     echo adding $val to the cluster
     con="$con      <connector-ref>$val-connector<\/connector-ref>\n"
   fi
done
sed -i "s/STAT_CON/$con/g" ${CONFIG_INSTANCE_DIR}/etc/broker.xml 

echo "#### Custom config done. ####"

